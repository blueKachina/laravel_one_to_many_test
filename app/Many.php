<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



/**
 * App\Many
 *
 * @property \Carbon\Carbon|null $created_at
 * @property int $id
 * @property string|null $name
 * @property int|null $ones_id
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\One|null $related_one
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Many whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Many whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Many whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Many whereOnesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Many whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Many extends Model
{

    protected $with = ['related_one'];
    //

    public function related_one()
    {
        return $this->belongsTo(One::class);
    }
    public function related_many()
    {
        return $this->belongsToMany(Many::class,'joins', 'manies_id','manies2_id')->withTimestamps();       //Important to add withTimestamps if you want them updated in your pivot table
    }
}
