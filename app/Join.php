<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * App\Join
 *
 * @property \Carbon\Carbon|null $created_at
 * @property int $id
 * @property int|null $manies2_id
 * @property int|null $manies_id
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Many $related_many
 * @property-read \App\Many|null $related_many2
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Join whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Join whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Join whereManies2Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Join whereManiesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Join whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Join extends Model
{

    protected $with = ['related_many' , 'related_many2'];

    //
    /**
     * Get the Many record associated with this join.
     */
    public function related_many()
    {
        return $this->belongsTo(Many::class);
    }
    /**
     * Get the ToMany record associated with this join.
     */
    public function related_many2()
    {
        return $this->belongsTo(Many::class,'manies2_id');
    }

}
