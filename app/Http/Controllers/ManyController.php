<?php

namespace App\Http\Controllers;

use App\Many;
use Illuminate\Http\Request;

class ManyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Many  $many
     * @return \Illuminate\Http\Response
     */
    public function show(Many $many)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Many  $many
     * @return \Illuminate\Http\Response
     */
    public function edit(Many $many)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Many  $many
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Many $many)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Many  $many
     * @return \Illuminate\Http\Response
     */
    public function destroy(Many $many)
    {
        //
    }
}
