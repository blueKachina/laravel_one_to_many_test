<?php

namespace App\Http\Controllers;

use App\Many;
use App\One;

class JoinController extends Controller
{

    /**
     * @return array
     */
    static function BeginTest(){

        $jsonOutput = array();

        $jsonOutput[] = static::ExampleOneToOne();
        $jsonOutput[] = static::ExampleOneToMany();
        $jsonOutput[] = static::ExampleManyToMany();

        return [$jsonOutput];

    }

    /**
     * @return One
     */
    static function ExampleOneToOne(){

        $exampleName = "One To One" ;

        $parent = new One();
        $parent->name = $exampleName;
        $parent->save();  //This is done so that we have an ID on the one record (this will be used to relate the many record later on)

        $child = new One();
        $child->name = $exampleName;


        //  This IF statement exists so that we are actually mandating a one to one relationship.
        $child = new One();
        $child->name = $exampleName;
        if ($parent->related_one === null)          //Check to see if a related record exists already
            $parent->related_one()->save($child); //This actually saves the child record, it will properly be related to its parent
        else
            $parent->related_one->update($child->toArray()); //If we chose to do a save here, it would actually create a 2nd child that would never be visible because this is supposed to be a 1:1 relationship

        //$one = One::findOrFail($one->id);                  //Force $one to be completely reloaded
        $parent->load('related_one');               //Refresh only the part of $one whose corresponding parts have been updated in the DB
        return $parent;
    }


    /**
     * @return One
     */
    static function ExampleOneToMany(){
        $exampleName = "One To Many" ;
        $parent = new One();
        $parent->name = $exampleName;
        $parent->save();  //This is done so that we have an ID on the one record (this will be used to relate the many record later on)

        $childArray = [];   //We will later be able to save an entire array of child objects using one command.  This will be that array of child objects
        for ($i = 0; $i<3 ; $i++){
            $child = new Many();
            $child->name = $exampleName . " $i";
            $childArray[] = $child;
        }

        $parent->related_many()->saveMany($childArray); //This saves the array of child objects in one fell swoop.  Each will be properly related to the $one (Neo)

        //$one = One::findOrFail($one->id);                  //Force $one to be completely reloaded
        $parent->load('related_many');               //Refresh only the part of $one whose corresponding parts have been updated in the DB
        return $parent;
    }


    /**
     * @return Many
     */
    static function ExampleManyToMany(){
        $exampleName = "Many To Many" ;
        $parent = new Many();
        $parent->name = $exampleName;
        $parent->save();  //This is done so that we have an ID on the parent record

        $childArray = [];   //We will later be able to save an entire array of child objects using one command.  This will be that array of child objects
        for ($i = 0; $i<3 ; $i++){
            $child = new Many();
            $child->name = $exampleName . " $i";
            $child->save();
            $childArray[] = $child->id;
        }

        $parent->related_many()->attach($childArray); //This is basically what will create the pivot table records.  "sync" could be used instead of attach here for different behaviour

        //$one = One::findOrFail($one->id);                  //Force $one to be completely reloaded
        $parent->load('related_many');               //Refresh only the part of $one whose corresponding parts have been updated in the DB
        return $parent;
    }

}
