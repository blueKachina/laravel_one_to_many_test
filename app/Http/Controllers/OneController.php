<?php

namespace App\Http\Controllers;

use App\One;
use Illuminate\Http\Request;

class OneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\One  $one
     * @return \Illuminate\Http\Response
     */
    public function show(One $one)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\One  $one
     * @return \Illuminate\Http\Response
     */
    public function edit(One $one)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\One  $one
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, One $one)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\One  $one
     * @return \Illuminate\Http\Response
     */
    public function destroy(One $one)
    {
        //
    }
}
