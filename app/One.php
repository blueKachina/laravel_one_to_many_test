<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



/**
 * App\One
 *
 * @property \Carbon\Carbon|null $created_at
 * @property int $id
 * @property int|null $manies_id
 * @property string|null $name
 * @property int|null $ones_id
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Many[] $related_many
 * @property-read \App\One $related_one
 * @method static \Illuminate\Database\Eloquent\Builder|\App\One whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\One whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\One whereManiesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\One whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\One whereOnesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\One whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class One extends Model
{

    protected $with = ['related_many', 'related_one'];
    protected $fillable = ['name'];


    public function related_many()
    {
        return $this->hasMany(Many::class,'ones_id');                   //Use hasMany whenever you're in a model representing the table that contains the primary key for this X To Many relationship
    }

    public function related_one()
    {
        return $this->hasOne(One::class,'ones_id','id');        //Use hasOne whenever you're in a model representing the table that contains the primary key for this X To One relationship
    }
}
